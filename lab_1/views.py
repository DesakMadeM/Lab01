from django.shortcuts import render
from datetime import datetime
# Enter your name here
mhs_name = 'Desak Made' # TODO Implement this
mhs_age = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(mhs_age)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    thisYear = datetime.now().year
    return thisYear-birth_year
